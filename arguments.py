import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--output', '-o', type=str, required=True,
                    help='Location of the CPM solution output. '
                         'Two files will be created: <output>_graph.pdf for the project graph and'
                         '<output>_gannt.pdf for the project Gantt chart.')
parser.add_argument('--levels', '-l', type=int, default=5,
                    help='Number of activity levels to generate. Default: 5')
parser.add_argument('--activities', '-a', type=int, default=3,
                    help='Max number of activities per level. Default 3. '
                         'Generated number of activities random, but between 1 and <activities>')
parser.add_argument('--dur-min', type=int, default=1,
                    help='Minimal activity duration of each activity')
parser.add_argument('--dur-max', type=int, default=7,
                    help='Maximal activity duration')
