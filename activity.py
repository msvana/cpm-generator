class Activity:
    """
    Represents one activity (or task) in a project.
    """
    def __init__(self, name, duration):
        """
        :param name: Unique name of the task (used as an ID graphviz node ID)
        :param duration: Duration of a task in some arbitrary units
        """
        self.name = name
        self.duration = duration

        self.predecessors = []
        self.successors = []

        self.earliest_start = self.earliest_finish = 0
        self.latest_finish = self.latest_start = 1000
        self.total_reserve = 0

    def __repr__(self):
        """
        Corresponds to one line in the activities table which will be given to a student
        """
        pred_names = ','.join(p.name for p in self.predecessors if p.name != 'START')
        return '%s:\t\t%d\t\t%s' % (self.name, self.duration, pred_names)

    def add_predecessor(self, predecessor):
        self.predecessors.append(predecessor)

    def add_successor(self, successor):
        self.successors.append(successor)

    def calculate_earliest(self):
        """
        Recursively calculates Earliest start and Earliest finish. Should be called on the FINISH
        node. Calls calculation procedure on each successor.
        """
        if len(self.predecessors) == 0:
            self.earliest_start = 0
        for p in self.predecessors:
            p.calculate_earliest()
            if p.earliest_finish > self.earliest_start:
                self.earliest_start = p.earliest_finish
        self.earliest_finish = self.earliest_start + self.duration

    def calculate_latest(self):
        """
        Recursively calculates Latest start and latest finish. Should be called on the START node.
        Invokes the same procedure on each successor.
        """
        if len(self.successors) == 0:
            self.latest_finish = self.earliest_finish
        for s in self.successors:
            s.calculate_latest()
            if s.latest_start < self.latest_finish:
                self.latest_finish = s.latest_start
        self.latest_start = self.latest_finish - self.duration
        self.total_reserve = self.latest_finish - self.earliest_finish
