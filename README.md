# CPM Exercise generator

This script generates a random set of project activities (tasks). Each activity in this 
set has random duration and random dependencies on previous activities. These random 
projects should serve as an assignement for students of the Quantitative Methods course
we teach at the 
[Faculty of Economics of Technical University of Ostrava](https://www.ekf.vsb.cz/en/). 

After the student receives the set of random project activities, they have to perform the
following tasks:

1. Draw the project graph
2. Calculate earliest start, earliest finish, latest start and latest finish for each
   activity
3. Calculate total time reserves and find the critical path
4. Draw the Gantt chart of the project

In addition to generating the assignment for the student, the script also prepares a
solution for all tasks. The teacher can then use the solution to quicky evaluate
students' work.

## Installation and requirements

You have to have [Python 3](https://www.python.org/) installed on your machine (tested on 
version 3.8). [Orca](https://github.com/plotly/orca) and 
[Graphviz](https://graphviz.org/download/) executables are also required. 

After these requirements are met, you can simply clone/download the repository. Then you
install the required Python libraries by running

```shell
$ cd /path/to/cpm_generator
$ pip install -r requirements.txt
```

## Running the script

You can run the generator by executing the following command:

```shell
python main.py -o solutions/1
```

You should be able to see the list of project activities for the student on the terminal
Additionally 2 more files containing the solution are generated: 

1. `solutions/1_graph.pdf` contains the project graph with the critical path highlighted
   in red. The graph also shows the earliest/latest start and finish of each activity, as
   well as the total time reserve.
2. `solutions/1_gantt.pdf` contains the Gantt chart of the project

You can of course change the `-o` parameter to select some other output path.

## TODO

- Add description of additional command line arguments to the README
- Add output examples to the README
- Add free reserves to the solution
- Generate assignment as an HTML document