import random
import string
from activity import Activity

import graphviz
import pandas
import plotly.express as px

import arguments


def main():
    args = arguments.parser.parse_args()

    # We simply name activities using the alphabet
    names_available = list(string.ascii_uppercase)

    all_activities = []
    start = Activity('START', 0)
    all_activities.append(start)

    # Stores all activities created at the previous level. Used to generate activity dependencies
    prev_level_activities = [start]

    for level in range(1, args.levels + 1):
        curr_level_activities = []

        for i in range(random.randint(1, args.activities)):
            activity = Activity(names_available.pop(0), random.randint(args.dur_min, args.dur_max))

            # Select random activity from previous level as predecessor
            first_pred_idx = random.randint(0, len(prev_level_activities) - 1)
            first_pred = prev_level_activities[first_pred_idx]
            first_pred.add_successor(activity)
            activity.add_predecessor(first_pred)

            # Select a random number of additional predecessors from the previous level
            for _ in range(len(prev_level_activities)):
                # Chance 2 in 3 to end the procedure
                if random.randint(1, 3) < 3:
                    break
                random_pred_idx = random.randint(0, len(prev_level_activities) - 1)
                if prev_level_activities[random_pred_idx] not in activity.predecessors:
                    prev_level_activities[random_pred_idx].add_successor(activity)
                    activity.add_predecessor(prev_level_activities[random_pred_idx])

            all_activities.append(activity)
            curr_level_activities.append(activity)

        prev_level_activities = curr_level_activities

    finish = Activity('FINISH', 0)
    all_activities.append(finish)

    # Add FINISH as successor to all activities with no other successor
    for a in all_activities:
        if len(a.successors) == 0 and a.name != 'FINISH':
            a.add_successor(finish)
            finish.add_predecessor(a)

    finish.calculate_earliest()
    start.calculate_latest()

    print_activities(all_activities)
    render_graph(all_activities, '%s_graph' % args.output)
    render_gantt(all_activities, '%s_gantt.pdf' % args.output)


def print_activities(activities):
    for a in activities:
        if a.name not in ['START', 'FINISH']:
            print(a)


def render_graph(activities, path):
    graph = graphviz.Digraph()

    for a in activities:
        if a.name == 'START':
            graph.node('START', 'START')
        elif a.name == 'FINISH':
            graph.attr('node', color='black')
            graph.node('FINISH', 'FINISH')
        else:
            # Critical path nodes will be in red
            if a.total_reserve == 0:
                graph.attr('node', color='red')
            else:
                graph.attr('node', color='black')

            graph.node(a.name, '%s(%d)|%d|%d\n%d|%d|%d'
                       % (a.name, a.duration, a.earliest_start, a.earliest_finish,
                          a.total_reserve, a.latest_start, a.latest_finish))

        # Add edges connections to all predecessors
        for p in a.predecessors:
            graph.edge(p.name, a.name)

    graph.render(path)


def render_gantt(activities, path):
    gantt_data = [{'activity': a.name, 'start': a.earliest_start, 'end': a.earliest_finish}
                  for a in activities if a.name not in ['START', 'FINISH']]
    gantt_df = pandas.DataFrame(gantt_data)
    gantt = px.timeline(gantt_df, x_start='start', x_end='end', y='activity')

    # This is needed to correctly display units of time
    gantt.layout.xaxis.type = 'linear'
    gantt.layout.xaxis.dtick = 1
    gantt.data[0].x = (gantt_df.end - gantt_df.start).tolist()

    image = gantt.to_image(format='pdf', width=800)
    image_file = open(path, 'wb')
    image_file.write(image)


if __name__ == '__main__':
    main()
